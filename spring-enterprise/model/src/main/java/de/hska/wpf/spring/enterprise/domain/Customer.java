package de.hska.wpf.spring.enterprise.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document
public class Customer extends AbstractModel
{
	private String			firstName, lastName;

	private Date			birthDate;

	private MailAddress		mailAddress;

	@DBRef
	private List<Address>	addresses	= new ArrayList<>();

	private int				rating;

	/**
	 * 
	 * @param firstName
	 *            - must not be {@literal null}
	 * @param lastName
	 *            - must not be {@literal null}
	 */
	public Customer( String firstName, String lastName )
	{
		Assert.hasText( firstName );
		Assert.hasText( lastName );

		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Customer( String firstName, String lastName, Date birthDate, MailAddress mailAddress,
			List<Address> addresses, int rating )
	{
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.mailAddress = mailAddress;
		this.addresses = addresses;
		this.rating = rating;
	}

	protected Customer()
	{
		// hide default constructor
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName( String firstName )
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName( String lastName )
	{
		this.lastName = lastName;
	}

	public Date getBirthDate()
	{
		return birthDate;
	}

	public void setBirthDate( Date birthDate )
	{
		this.birthDate = birthDate;
	}

	public List<Address> getAddresses()
	{
		return addresses;
	}

	public void add( Address address )
	{
		Assert.notNull( address );
		this.addresses.add( address );
	}

	public int getRating()
	{
		return rating;
	}

	public void setRating( int rating )
	{
		this.rating = rating;
	}

	public MailAddress getMailAddress()
	{
		return mailAddress;
	}

	public void setMailAddress( MailAddress mailAddress )
	{
		this.mailAddress = mailAddress;
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( !( o instanceof Customer ) )
			return false;

		Customer customer = (Customer) o;

		if ( rating != customer.rating )
			return false;
		if ( !addresses.equals( customer.addresses ) )
			return false;
		if ( !birthDate.equals( customer.birthDate ) )
			return false;
		if ( !firstName.equals( customer.firstName ) )
			return false;
		if ( !lastName.equals( customer.lastName ) )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = firstName.hashCode();
		result = 31 * result + lastName.hashCode();
		result = 31 * result + birthDate.hashCode();
		result = 31 * result + addresses.hashCode();
		result = 31 * result + rating;
		return result;
	}

	@Override
	public String toString()
	{
		return "Customer{" + "firstName='" + firstName + '\'' + ", lastName='" + lastName + '\''
				+ ", birthDate=" + birthDate + ", rating=" + rating + '}';
	}
}
