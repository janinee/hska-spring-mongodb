package de.hska.wpf.spring.enterprise.domain;

import org.springframework.util.Assert;

import java.util.regex.Pattern;

public class MailAddress
{
	private static final String		EMAIL_REGEX	= "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	private static final Pattern	PATTERN		= Pattern.compile( EMAIL_REGEX );

	private String					value;

	public MailAddress( String mailAddress )
	{
		Assert.isTrue( isValid( mailAddress ) );
		this.value = mailAddress;
	}

	protected MailAddress()
	{
		// hide default constructor
	}

	public static boolean isValid( String candidate )
	{
		return candidate == null ? false : PATTERN.matcher( candidate ).matches();
	}

	@Override
	public String toString()
	{
		return value;
	}

	@Override
	public boolean equals( Object obj )
	{

		if ( this == obj )
		{
			return true;
		}

		if ( !( obj instanceof MailAddress ) )
		{
			return false;
		}

		MailAddress that = (MailAddress) obj;
		return this.value.equals( that.value );
	}

	@Override
	public int hashCode()
	{
		return value.hashCode();
	}
}