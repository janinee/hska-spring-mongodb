package de.hska.wpf.spring.enterprise.domain;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class AbstractModel
{
	@Id
	private ObjectId	id;

	public AbstractModel( ObjectId id )
	{
		this.id = id;
	}

	protected AbstractModel()
	{
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( !( o instanceof AbstractModel ) )
			return false;

		AbstractModel that = (AbstractModel) o;

		if ( !id.equals( that.id ) )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		return id.hashCode();
	}

	public ObjectId getId()
	{

		return id;
	}

	public void setId( ObjectId id )
	{
		this.id = id;
	}

}
