package de.hska.wpf.spring.enterprise.domain;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface CustomerRepository extends MongoRepository<Customer, String>
{
	@Query( value = "{ 'firstName' : ?0 }", fields = "{ 'firstName' : 1, 'lastName' : 1}" )
	List<Customer> findCustomersByFirstName( String firstname );

	List<Customer> findByFirstName( String firstname );
}
