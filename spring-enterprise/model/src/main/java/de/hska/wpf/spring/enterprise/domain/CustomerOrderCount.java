package de.hska.wpf.spring.enterprise.domain;

public class CustomerOrderCount
{
	private Customer	customer;

	private float		count;

	public Customer getCustomer()
	{
		return customer;
	}

	public void setCustomer( Customer customer )
	{
		this.customer = customer;
	}

	public float getCount()
	{
		return count;
	}

	public void setCount( float count )
	{
		this.count = count;
	}

	@Override
	public String toString()
	{
		return "CustomerOrderCount [customer=" + customer + ", count=" + count + "]";
	}
}
