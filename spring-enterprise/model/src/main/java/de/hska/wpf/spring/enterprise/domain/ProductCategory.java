package de.hska.wpf.spring.enterprise.domain;

public enum ProductCategory
{
	CLOTHES( "clothes & more" ), ELECTRONICS( "electronics & stuff" ), MISC( "all the rest." );

	private String	descriptionValue;

	private ProductCategory( String descriptionValue )
	{
		this.descriptionValue = descriptionValue;
	}

	public String getDescriptionValue()
	{
		return this.descriptionValue;
	}

}
