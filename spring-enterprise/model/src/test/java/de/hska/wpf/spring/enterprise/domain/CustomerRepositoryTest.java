package de.hska.wpf.spring.enterprise.domain;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "classpath:applicationConfig.xml" } )
public class CustomerRepositoryTest
{
	private final static String	CUSTOMER_FIRSTNAME	= "Gomez";

	private static final Log	log					= LogFactory
															.getLog( CustomerRepositoryTest.class );

	@Autowired
	private CustomerRepository	repository;

	@Autowired
	private MongoOperations		mongoOps;

	@Before
	public void initializeEntry()
	{
		Customer customer = new Customer( CUSTOMER_FIRSTNAME, "Gonsalez", new GregorianCalendar(
				1967, 5, 27 ).getTime(), new MailAddress( "Gonsalez@gmx.net" ), null, 4 );
		mongoOps.insert( customer );
	}

	@After
	public void dropCollections()
	{
		mongoOps.dropCollection( "customer" );
	}

	@Test
	public void readCustomersByFirstnameWithNullValues()
	{
		List<Customer> customer = repository.findCustomersByFirstName( CUSTOMER_FIRSTNAME );
		log.info( "WITH NULL: " + customer );
		Assert.assertEquals( CUSTOMER_FIRSTNAME, customer.get( 0 ).getFirstName() );
		Assert.assertNull( customer.get( 0 ).getBirthDate() );
	}

	@Test
	public void readCustomersByFirstname()
	{
		List<Customer> customer = repository.findByFirstName( CUSTOMER_FIRSTNAME );
		log.info( "WITHOUT NULL: " + customer );
		Assert.assertEquals( CUSTOMER_FIRSTNAME, customer.get( 0 ).getFirstName() );
	}
}
