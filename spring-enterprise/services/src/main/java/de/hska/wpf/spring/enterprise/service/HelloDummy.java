package de.hska.wpf.spring.enterprise.service;

import org.springframework.stereotype.Component;

@Component( "dummy" )
public class HelloDummy
{
	public String getHello()
	{
		return "Hello";
	}
}
