package de.hska.wpf.spring.enterprise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HelloService
{
	@Autowired
	private HelloDummy	hello;

	public String getHelloWorld()
	{
		return hello.getHello() + " " + "World";
	}
}
