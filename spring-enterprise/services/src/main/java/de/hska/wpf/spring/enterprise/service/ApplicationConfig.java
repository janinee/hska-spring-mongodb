package de.hska.wpf.spring.enterprise.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

@Configuration
@ComponentScan( basePackages = { "de.hska.wpf.spring.enterprise" } )
public class ApplicationConfig
{
	public @Bean
	Mongo mongo() throws Exception
	{
		return new MongoClient( "localhost" );
	}

	public @Bean
	MongoTemplate mongoTemplate() throws Exception
	{
		return new MongoTemplate( mongo(), "easpringdb" );
	}
}
