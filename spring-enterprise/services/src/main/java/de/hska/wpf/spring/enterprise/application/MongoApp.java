package de.hska.wpf.spring.enterprise.application;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.core.query.Update.update;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.mapreduce.GroupBy;
import org.springframework.data.mongodb.core.mapreduce.GroupByResults;

import de.hska.wpf.spring.enterprise.domain.Address;
import de.hska.wpf.spring.enterprise.domain.Customer;
import de.hska.wpf.spring.enterprise.domain.CustomerOrderCount;
import de.hska.wpf.spring.enterprise.domain.Item;
import de.hska.wpf.spring.enterprise.domain.MailAddress;
import de.hska.wpf.spring.enterprise.domain.Order;
import de.hska.wpf.spring.enterprise.domain.ProductCategory;
import de.hska.wpf.spring.enterprise.service.ApplicationConfig;

public class MongoApp
{
	private static final Log	log	= LogFactory.getLog( MongoApp.class );

	/**
	 * Main method to execute some queries
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main( String[] args ) throws Exception
	{
		MongoOperations mongoOps = new ApplicationConfig().mongoTemplate();

		createDbEntries( mongoOps );

		showPagination( mongoOps );
		showGroupBy( mongoOps );
		showAggregation( mongoOps );
		showSomeQueries( mongoOps );

		deleteCollections( mongoOps );
	}

	/**
	 * Uses MongoDb aggregation function to group by customer and count their
	 * orders.
	 * 
	 * @param mongoOps
	 */
	private static void showAggregation( MongoOperations mongoOps )
	{
		Aggregation agg = newAggregation( Order.class, group( "customer" ).count().as( "count" ),
				project( "count" ).and( "customer" ).previousOperation(),
				sort( Sort.Direction.DESC, "count" ) );

		AggregationResults<CustomerOrderCount> aggregationResults = mongoOps.aggregate( agg,
				"order", CustomerOrderCount.class );
		List<CustomerOrderCount> mappedResults = aggregationResults.getMappedResults();

		for ( CustomerOrderCount count : mappedResults )
		{
			log.info( "AGGREGATION: " + count.getCustomer().getFirstName() + " - "
					+ count.getCount() );
		}
	}

	/**
	 * Uses the group by function to group by customers and count their orders.
	 * 
	 * @param mongoOps
	 */
	private static void showGroupBy( MongoOperations mongoOps )
	{
		GroupByResults<CustomerOrderCount> groupByResults = mongoOps.group(
				"order",
				GroupBy.key( "customer" ).initialDocument( "{ count:0 }" )
						.reduceFunction( "function(doc, prev) { prev.count += 1 }" ),
				CustomerOrderCount.class );

		for ( CustomerOrderCount count : groupByResults )
		{
			log.info( "COUNT: " + count.getCustomer().getFirstName() + " - " + count.getCount() );
		}
	}

	/**
	 * Executes some standard queries like find, update and remove.
	 * 
	 * @param mongoOps
	 */
	private static void showSomeQueries( MongoOperations mongoOps )
	{
		Customer customer = mongoOps.findOne( query( where( "firstName" ).is( "Carrillo" ) ),
				Customer.class );
		List<Item> allClothesItems = mongoOps.find(
				query( where( "category" ).is( ProductCategory.CLOTHES ) ), Item.class );
		List<Order> findOrdersByCustomer = mongoOps.find(
				query( where( "customer" ).is( customer ) ), Order.class );
		List<Item> findAllItems = mongoOps.findAll( Item.class );

		log.info( "ONE CUSTOMER: " + customer );
		log.info( "ALL CLOTHES ITEMS: " + allClothesItems );
		log.info( "ORDERS BY CUSTOMER: " + findOrdersByCustomer );
		log.info( "ALL ITEMS: " + findAllItems );

		mongoOps.updateFirst( query( where( "firstName" ).is( "Carrillo" ) ),
				update( "rating", 5 ), Customer.class );
		customer = mongoOps
				.findOne( query( where( "firstName" ).is( "Carrillo" ) ), Customer.class );

		log.info( "CUSTOMER UPDATED: " + customer );

		mongoOps.remove( customer );
		List<Customer> customers = mongoOps.findAll( Customer.class );
		log.info( "CUSTOMER SIZE AFTER REMOVE: " + customers.size() );
	}

	/**
	 * Pagination example.
	 * 
	 * @param mongoOps
	 */
	private static void showPagination( MongoOperations mongoOps )
	{
		List<Customer> customerPaginationFirst = mongoOps.find( query( where( "rating" ).in( 7 ) )
				.skip( 0 ).limit( 1 ), Customer.class );

		List<Customer> customerPaginationSecond = mongoOps.find( query( where( "rating" ).in( 7 ) )
				.skip( 1 ).limit( 1 ), Customer.class );

		for ( Customer customer : customerPaginationFirst )
		{
			log.info( "NAVIGATION FIRST PAGE: Name: " + customer.getFirstName() );
		}
		for ( Customer customer : customerPaginationSecond )
		{
			log.info( "NAVIGATION SECOND PAGE: Name: " + customer.getFirstName() );
		}
	}

	/**
	 * Inserts some sample data in the database
	 * 
	 * @param mongoOps
	 */
	private static void createDbEntries( MongoOperations mongoOps )
	{
		Address address1 = new Address( "P.O. Box 208, 6154 Et Road", 189,
				"luctus ut, pellentesque", "99058", "Tollembeek", "Palau" );
		Address address2 = new Address( "Ap #253-6900 Quis, Rd.", 150, "id, blandit at,", "1674",
				"Contagem", "Lesotho" );
		Address address3 = new Address( "3751 Est St.", 7, "enim, condimentum eget,", "55685",
				"Wijshagen", "Poland" );
		mongoOps.insert( address1 );
		mongoOps.insert( address2 );
		mongoOps.insert( address3 );

		ArrayList<Address> listOfAddressesWithOneEntry = new ArrayList<Address>();
		listOfAddressesWithOneEntry.add( address1 );
		ArrayList<Address> listOfAddressesWithTwoEntries = new ArrayList<Address>();
		listOfAddressesWithTwoEntries.add( address1 );
		listOfAddressesWithTwoEntries.add( address2 );
		ArrayList<Address> listOfAddressesWithThreeEntries = new ArrayList<Address>();
		listOfAddressesWithThreeEntries.add( address1 );
		listOfAddressesWithThreeEntries.add( address2 );
		listOfAddressesWithThreeEntries.add( address3 );

		MailAddress mailAddress1 = new MailAddress( "vulputate.mauris@urnac.com" );
		MailAddress mailAddress2 = new MailAddress( "volutpat.ornare@amet.com" );
		MailAddress mailAddress3 = new MailAddress( "pede@dolorsit.org" );

		Customer customer1 = new Customer( "Carrillo", "Hilda",
				new GregorianCalendar( 1969, 3, 2 ).getTime(), mailAddress1,
				listOfAddressesWithOneEntry, 9 );
		Customer customer2 = new Customer( "Montgomery", "Ursa",
				new GregorianCalendar( 1962, 1, 8 ).getTime(), mailAddress2,
				listOfAddressesWithTwoEntries, 7 );
		Customer customer3 = new Customer( "Gomez", "Rhona",
				new GregorianCalendar( 2004, 9, 28 ).getTime(), mailAddress3,
				listOfAddressesWithThreeEntries, 7 );
		mongoOps.insert( customer1 );
		mongoOps.insert( customer2 );
		mongoOps.insert( customer3 );

		Item item1 = new Item( ProductCategory.CLOTHES, "Shirt", "Shirt mit kurzen �rmeln",
				new BigDecimal( 10 ) );
		Item item2 = new Item( ProductCategory.ELECTRONICS, "S5", "Samsung Galaxy S5",
				new BigDecimal( 650 ) );
		Item item3 = new Item( ProductCategory.MISC, "Uhu", "Guter Klebestoff", new BigDecimal( 3 ) );
		mongoOps.insert( item1 );
		mongoOps.insert( item2 );
		mongoOps.insert( item3 );

		ArrayList<Item> itemListWithOneEntry = new ArrayList<Item>();
		itemListWithOneEntry.add( item1 );
		ArrayList<Item> itemListWithTwoEntries = new ArrayList<Item>();
		itemListWithTwoEntries.add( item1 );
		itemListWithTwoEntries.add( item2 );
		ArrayList<Item> itemListWithThreeEntries = new ArrayList<Item>();
		itemListWithThreeEntries.add( item1 );
		itemListWithThreeEntries.add( item2 );
		itemListWithThreeEntries.add( item3 );

		Order order1 = new Order( customer2, itemListWithOneEntry );
		Order order2 = new Order( customer1, itemListWithTwoEntries );
		Order order3 = new Order( customer1, itemListWithThreeEntries );
		mongoOps.insert( order1 );
		mongoOps.insert( order2 );
		mongoOps.insert( order3 );
	}

	/**
	 * Removes the created collections from the database.
	 * 
	 * @param mongoOps
	 */
	private static void deleteCollections( MongoOperations mongoOps )
	{
		mongoOps.dropCollection( "customer" );
		mongoOps.dropCollection( "address" );
		mongoOps.dropCollection( "item" );
		mongoOps.dropCollection( "order" );
	}
}
