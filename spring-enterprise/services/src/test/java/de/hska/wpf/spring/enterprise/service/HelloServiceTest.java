package de.hska.wpf.spring.enterprise.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith( SpringJUnit4ClassRunner.class )
// @ContextConfiguration(locations = {"classpath:spring/applicationConfig.xml"})
@ContextConfiguration( classes = { ApplicationConfig.class } )
public class HelloServiceTest
{

	@Autowired
	private HelloService	helloService;

	@Test
	public void helloWorld()
	{
		Assert.assertTrue( helloService.getHelloWorld().equals( "Hello World" ) );
	}
}
