package de.hska.wpf.spring.enterprise.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith( SpringJUnit4ClassRunner.class )
// @ContextConfiguration(locations = {"classpath:spring/applicationConfig.xml"})
@ContextConfiguration( classes = { ApplicationConfig.class } )
public class HelloDummyTest
{
	private HelloDummy	helloDummy	= new HelloDummy();

	@Autowired
	@Qualifier( "dummy" )
	private HelloDummy	helloDummyInjected;

	@Test
	public void instanceTest()
	{
		Assert.assertEquals( "Muss 'Hello' zurückliefern!", "Hello", helloDummy.getHello() );
	}

	@Test
	public void injectedTest()
	{
		Assert.assertEquals( "Muss 'Hello' zurückliefern!", "Hello", helloDummyInjected.getHello() );
	}
}
